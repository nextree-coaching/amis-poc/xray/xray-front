import * as React from 'react';
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import {styled} from "@mui/material/styles";
import MuiAppBar from "@mui/material/AppBar";
import Badge from "@mui/material/Badge";
import NotificationsIcon from "@mui/icons-material/Notifications";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";

const ITEM_HEIGHT = 90;

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({theme, open}) => ({
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

export default function AppBarView(props) {
    const {deadMessages, onSelect} = props

    const [open, setOpen] = React.useState(true);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const openNoty = Boolean(anchorEl);
    const toggleDrawer = () => {
        setOpen(!open);
    };

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <React.Fragment>
            <AppBar position="absolute" open={open}>
                <Toolbar
                    sx={{
                        pr: '24px', // keep right padding when drawer closed
                    }}
                >
                    <Typography
                        component="h1"
                        variant="h6"
                        color="inherit"
                        noWrap
                        sx={{flexGrow: 1}}
                    >
                        Dashboard
                    </Typography>
                    <IconButton
                        color="inherit"
                        aria-label="more"
                        id="long-button"
                        aria-controls={openNoty ? 'long-menu' : undefined}
                        aria-expanded={openNoty ? 'true' : undefined}
                        aria-haspopup="true"
                        onClick={handleClick}
                    >
                        <Badge badgeContent={deadMessages.length} color="secondary">
                            <NotificationsIcon/>
                        </Badge>
                    </IconButton>
                    <Menu
                        id="long-menu"
                        MenuListProps={{
                            'aria-labelledby': 'long-button',
                        }}
                        anchorEl={anchorEl}
                        open={openNoty}
                        onClose={handleClose}
                        PaperProps={{
                            style: {
                                maxHeight: ITEM_HEIGHT * 4.5,
                                width: '60ch',
                            },
                        }}
                    >
                        {deadMessages.map((row) => (
                            <>
                                <MenuItem key={row.id} onClick={() => onSelect(row.trailInfo.trailId)}>
                                    [{row.trailInfo.service.replace("kr.amc.cloud.amis.", "")}] <br/>
                                    - message : {row.trailInfo.message}<br/>
                                    - id : {row.id}<br/>
                                </MenuItem>
                                <hr/>
                            </>
                        ))}
                    </Menu>
                </Toolbar>
            </AppBar>
        </React.Fragment>
    );
}