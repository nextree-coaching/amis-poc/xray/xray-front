import * as React from 'react';
import Title from './Title';
import Box from "@mui/material/Box";
import {DataGrid} from "@mui/x-data-grid";

const columns = [
    {field: 'id', headerName: 'ID', width: 90},
    {
        field: 'payloadType',
        headerName: 'PayloadType',
        width: 150,
    },
    {
        field: 'payload',
        headerName: 'Payload',
        width: 150,
    },
    {
        field: 'service',
        headerName: 'Service',
        width: 110,
    },
    {
        field: 'timestamp',
        headerName: 'Timestamp',
        width: 110,
    },
    {
        field: 'trailId',
        headerName: 'TrailId',
        width: 110,
    },
    {
        field: 'messageId',
        headerName: 'MessageId',
        width: 110,
    },
    {
        field: 'message',
        headerName: 'Message',
        width: 110,
    },
    {
        field: 'parentMessageId',
        headerName: 'ParentMessageId',
        width: 110,
    },
    {
        field: 'parentService',
        headerName: 'ParentService',
        width: 110,
    }, {
        field: 'parentMessage',
        headerName: 'ParentMessage',
        width: 110,
    }, {
        field: 'userId',
        headerName: 'UserId',
        width: 110,
    },
    {
        field: 'messageType',
        headerName: 'MessageType',
        width: 110,
    },
    {
        field: 'requestTime',
        headerName: 'RequestTime',
        type: 'number',
        width: 110,
    },
    {
        field: 'waitingTime',
        headerName: 'WaitingTime',
        type: 'number',
        width: 110,
    },
];

export default function FlowView(props) {
    const {selectRow} = props;

    console.log(selectRow);
    return (
        <React.Fragment>
            <Title>Flow</Title>
            <Box sx={{ height: 350, width: '100%' }}>
                <DataGrid
                    rows={selectRow.map((data)=>({...data, ...data.streamEvent,...data.streamEvent.trailInfo}))}
                    rowHeight={38}
                    columns={columns}
                    pageSize={5}
                    rowsPerPageOptions={[5]}
                    disableSelectionOnClick
                    experimentalFeatures={{ newEditingApi: true }}
                />
            </Box>
        </React.Fragment>
    );
}