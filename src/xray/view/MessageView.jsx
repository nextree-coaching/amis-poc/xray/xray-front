import * as React from 'react';
import Title from './Title';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableContainer from '@mui/material/TableContainer';
import TablePagination from '@mui/material/TablePagination';
import Paper from '@mui/material/Paper';
import { useState } from "react";

const columns = [
    {field: 'id', headerName: 'ID', width: 90},
    {
        field: 'payloadType',
        headerName: 'PayloadType',
        width: 150,
    },
    {
        field: 'payload',
        headerName: 'Payload',
        width: 150,
    },
    {
        field: 'service',
        headerName: 'Service',
        width: 110,
    },
    {
        field: 'timestamp',
        headerName: 'Timestamp',
        width: 110,
    },
    {
        field: 'trailId',
        headerName: 'TrailId',
        width: 110,
    },
    {
        field: 'messageId',
        headerName: 'MessageId',
        width: 110,
    },
    {
        field: 'message',
        headerName: 'Message',
        width: 110,
    },
    {
        field: 'parentMessageId',
        headerName: 'ParentMessageId',
        width: 110,
    },
    {
        field: 'parentService',
        headerName: 'ParentService',
        width: 110,
    }, {
        field: 'parentMessage',
        headerName: 'ParentMessage',
        width: 110,
    }, {
        field: 'userId',
        headerName: 'UserId',
        width: 110,
    },
    {
        field: 'messageType',
        headerName: 'MessageType',
        width: 110,
    },
    {
        field: 'requestTime',
        headerName: 'RequestTime',
        type: 'number',
        width: 110,
    },
    {
        field: 'waitingTime',
        headerName: 'WaitingTime',
        type: 'number',
        width: 110,
    },
];


export default function MessageView(props) {
    const {gridRows, onSelect} = props;




    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    return (
        <React.Fragment>
            <Paper sx={{ width: '100%', overflow: 'hidden' }}>
            <Title>Message</Title>
            <TableContainer sx={{ maxHeight: 440 }}>
                <Table>
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell key={column.id}>
                                    {column.headerName}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {gridRows.map((row) => (
                            <TableRow onClick={() => onSelect(row.trailId)} key={row.id}>
                                <TableCell>{row.id}</TableCell>
                                <TableCell>{row.payloadClass}</TableCell>
                                <TableCell>{row.payload}</TableCell>
                                <TableCell>{row.timestamp}</TableCell>
                                <TableCell>{row.trailId}</TableCell>
                                <TableCell>{row.messageId}</TableCell>
                                <TableCell>{row.service}</TableCell>
                                <TableCell>{row.message}</TableCell>
                                <TableCell>{row.parentMessageId}</TableCell>
                                <TableCell>{row.parentService}</TableCell>
                                <TableCell>{row.parentMessage}</TableCell>
                                <TableCell>{row.userId}</TableCell>
                                <TableCell>{row.messageType}</TableCell>
                                <TableCell>{row.requestTime}</TableCell>
                                <TableCell>{row.waitingTime}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={gridRows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
            </Paper>
        </React.Fragment>
    );
}