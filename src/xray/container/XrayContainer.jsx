import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import AppBarView from "../view/AppBarView";
import Toolbar from "@mui/material/Toolbar";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import MessageView from "../view/MessageView";
import FlowView from "../view/FlowView";
import {createTheme, ThemeProvider} from "@mui/material/styles";
import { Client } from '@stomp/stompjs';
import * as React from "react";
import {useEffect, useState} from "react";
import axios from "axios";

const mdTheme = createTheme();
const SOCKET_URL = 'ws://localhost:9100/ws-message';


function XrayContainer() {
    const [selectRow, setSelectRow] = useState([]);
    const [gridRows, setGridRows] = useState([]);
    const [deadMessages, setDeadMessages] = useState([]);


    const api = async (api, body) => {
        const result = await axios.post(api,body || {}, {
            header:{
                'accept': '*/*',
                "Content-Type": `application/json`,
            }
        }).then( response => response.data || [] );

        return result.queryResult;
    }

    const init = async () =>{
        const deadLetter = await api("/api/xray/flow/streamevent/dead-letter-message");
        const streamEvent = await api("/api/xray/flow/streamevent/stream-event");
        setDeadMessages(deadLetter.map(({deadLetterMessage})=>deadLetterMessage));
        setGridRows(streamEvent.map((data)=>({...data, ...(data.streamEvent||{}),...(data.streamEvent.trailInfo||{})})));
    }

    useEffect(()=>{
        init();
    },[])


    let onConnected = () => {
        console.log("Connected!!")
        client.subscribe('/topic/dead/message', function (msg) {
            console.log('msg>>>>>>>>>>', msg)
            if (msg) {
                let json = JSON.parse(msg);
                // if (json.message) {
                    setDeadMessages(json)
                    console.log('json>>>>>>>>>>', json)
                // }
            }
        })
    }
    let onDisconnected = () => {
        console.log("Disconnected!!")
    }

    const client = new Client({
        brokerURL: SOCKET_URL,
        reconnectDelay: 5000,
        heartbeatIncoming: 4000,
        heartbeatOutgoing: 4000,
        onConnect: onConnected,
        onDisconnect: onDisconnected
    });

    const onChangeTrailId = async (trailId) =>{
        const result = await api("/api/xray/flow/streamevent/related-message", { trailId });
        setSelectRow(result);
    }

    useEffect(() => {
        init();
    },[])

    client.activate();

    return (
        <ThemeProvider theme={mdTheme}>
            <Box sx={{ display: 'flex' }}>
                <CssBaseline />
                <AppBarView deadMessages={deadMessages} onSelect={onChangeTrailId}/>
                <Box
                    component="main"
                    sx={{
                        backgroundColor: (theme) =>
                            theme.palette.mode === 'light'
                                ? theme.palette.grey[100]
                                : theme.palette.grey[900],
                        flexGrow: 1,
                        height: '100vh',
                        overflow: 'auto',
                    }}
                >
                    <Toolbar />
                    <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
                        <Grid container spacing={2}>
                            {/* Recent MessageView */}
                            <Grid item xs={12}>
                                <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                                    <MessageView gridRows={gridRows} onSelect={onChangeTrailId}/>
                                </Paper>
                            </Grid>
                            {/* Recent FlowView */}
                            <Grid item xs={12}>
                                <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                                    <FlowView selectRow={selectRow}/>
                                </Paper>
                            </Grid>
                            {/* Recent MessageView */}
                            <Grid item xs={12}>
                                <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                                </Paper>
                            </Grid>
                        </Grid>
                    </Container>
                </Box>
            </Box>
        </ThemeProvider>
    )
}

export default XrayContainer;