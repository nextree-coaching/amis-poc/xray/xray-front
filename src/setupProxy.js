const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = (app) => {
    app.use(
        createProxyMiddleware(
            ['/api/xray'],
            {
                target: "http://localhost:9100",
                changeOrigin: true,
                pathRewrite: {
                    '/api/xray': ""
                }
            }
        )
    );
}